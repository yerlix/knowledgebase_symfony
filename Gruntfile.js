module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compass : {
            dist: {
                options: {
                    sassDir : 'src/KnowledgeBase/Bundle/CoreBundle/Resources/public/sass/',
                    cssDir : 'web/assets/css/',
                    environment: 'development'
                }
            }
        },
        bowercopy: {
            options: {
                destPrefix: 'web/assets'
            },
            scripts: {
                files: {
                    'js/jquery.js': 'app/Resources/public/vendor/jquery/dist/jquery.min.js',
                    'js/jquery-ui.js': 'app/Resources/public/vendor/jquery-ui/jquery-ui.min.js',
                    'js/bootstrap.js': 'app/Resources/public/vendor/bootstrap-sass/assets/javascripts/bootstrap.min.js',
                    'js/highlight.js': 'app/Resources/public/vendor/highlightjs/highlight.pack.js'
                }
            },
            fonts: {
                files: {
                    'fonts': 'app/Resources/public/vendor/bootstrap-sass/assets/fonts/bootstrap/'
                }
            },
            css: {
                files: {
                    'css/highlight.css': 'app/Resources/public/vendor/highlightjs/styles/monokai_sublime.css'
                }
            }
        },
        concat: {
            options: {
                stripBanners: true
            },
            css: {
                src: [
                    'web/assets/css/base.css',
                    'web/assets/css/highlight.css'
                ],
                dest: 'web/assets/css/bundled.css'
            },
            js : {
                src : [
                    'web/assets/js/jquery.js',
                    'web/assets/js/bootstrap.js',
                    'web/assets/js/highlight.js',
                    'src/KnowledgeBase/Bundle/CoreBundle/Resources/public/js/*.js'
                ],
                dest: 'web/assets/js/bundled.js'
            }
        },
        cssmin : {
            bundled:{
                src: 'web/assets/css/bundled.css',
                dest: 'web/assets/css/bundled.min.css'
            }
        },
        uglify : {
            js: {
                files: {
                    'web/assets/js/bundled.min.js': ['web/assets/js/bundled.js']
                }
            }
        },
        watch: {
            css: {
                files: ['src/KnowledgeBase/Bundle/CoreBundle/Resources/public/sass/*.scss', 'src/KnowledgeBase/Bundle/CoreBundle/Resources/public/js/*.js'],
                tasks: ['compass', 'concat', 'cssmin', 'uglify']
            }
        }
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-css-url-rewrite");

    grunt.registerTask('default', ['compass','bowercopy', 'concat', 'cssmin', 'uglify']);
    grunt.registerTask('dev', ['compass', 'concat', 'uglify', 'cssmin', 'watch']);
};
