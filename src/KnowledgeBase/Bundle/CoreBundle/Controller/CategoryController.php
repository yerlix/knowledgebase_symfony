<?php

namespace KnowledgeBase\Bundle\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use KnowledgeBase\Bundle\CoreBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CategoryController
 * @Configuration\Route(service="knowledgebase_core.controller.category_controller")
 */
class CategoryController
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var FlashBagInterface
     */
    protected $flashBag;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var EntityRepository
     */
    protected $snippetRepository;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FlashBagInterface $flashBag
     * @param RouterInterface $router
     * @param EntityRepository $snippetRepository
     */
    function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FlashBagInterface $flashBag,
        RouterInterface $router,
        EntityRepository $snippetRepository
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->snippetRepository = $snippetRepository;
    }

    /**
     * @Configuration\Route("/category/{categoryId}", name="category_index", requirements={"categoryId": "\d+"})
     * @Configuration\Template
     * @Configuration\ParamConverter("category", options={"mapping": {"categoryId": "id"}})
     */
    public function indexAction(Category $category)
    {
        $snippets = $this->snippetRepository->findBy(array('category' => $category));

        return array(
            'category' => $category,
            'snippets' => $snippets
        );
    }

    /**
     * @Configuration\Route("/category/create", name="category_create")
     * @Configuration\Template
     */
    public function createAction(Request $request)
    {
        $category = new Category();

        $form = $this->formFactory->create('knowledgebase_core_category', $category);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Category successfully created.');

            return new RedirectResponse($this->router->generate(
                'category_edit',
                array('categoryId' => $category->getId())
            ));
        }

        return array(
            'category' => $category,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/category/edit/{categoryId}", name="category_edit")
     * @Configuration\Template
     * @Configuration\ParamConverter("category", options={"mapping": {"categoryId": "id"}})
     */
    public function editAction(Request $request, Category $category)
    {
        $form = $this->formFactory->create('knowledgebase_core_category', $category);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($category);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Category successfully updated.');

            return new RedirectResponse($this->router->generate(
                'category_edit',
                array('categoryId' => $category->getId())
            ));
        }

        return array(
            'category' => $category,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/category/delete/{categoryId}", name="category_delete")
     * @Configuration\Template
     * @Configuration\ParamConverter("category", options={"mapping": {"categoryId": "id"}})
     */
    public function deleteAction(Request $request, Category $category)
    {
        $this->entityManager->remove($category);
        $this->entityManager->flush();

        $this->flashBag->set('success', 'Category successfully deleted.');

        return new RedirectResponse($this->router->generate('home'));
    }
}
