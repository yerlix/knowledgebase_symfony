<?php

namespace KnowledgeBase\Bundle\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration;

/**
 * class CoreController
 *
 * @Configuration\Route(service="knowledgebase_core.controller.core_controller")
 */
class CoreController
{
    /**
     * @Configuration\Route("/", name="home")
     * @Configuration\Template
     */
    public function indexAction()
    {
        return array(
        );
    }
}
