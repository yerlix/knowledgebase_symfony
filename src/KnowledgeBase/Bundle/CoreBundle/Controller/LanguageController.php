<?php
namespace KnowledgeBase\Bundle\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use KnowledgeBase\Bundle\CoreBundle\Entity\Language;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class LanguageController
 *
 * @Configuration\Route(service="knowledgebase_core.controller.language_controller")
 */
class LanguageController
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityRepository
     */
    private $languageRepository;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FlashBagInterface $flashBag
     * @param RouterInterface $router
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FlashBagInterface $flashBag,
        RouterInterface $router,
        EntityRepository $languageRepository
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @Configuration\Route("/languages", name="language_index")
     * @Configuration\Template
     */
    public function indexAction(Request $request)
    {
        $languages = $this->languageRepository->findAll();

        return array(
            'languages' => $languages
        );
    }

    /**
     * @Configuration\Route("/language/create", name="language_create")
     * @Configuration\Template
     */
    public function createAction(Request $request)
    {
        $language = new Language();

        $form = $this->formFactory->create('knowledgebase_core_language', $language);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($language);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Language successfully created');

            return new RedirectResponse($this->router->generate(
                'language_edit',
                array('langId' => $language->getId())
            ));
        }


        return array(
            'language' => $language,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/language/edit/{langId}", name="language_edit")
     * @Configuration\Template
     * @configuration\ParamConverter("language", options={"mapping": {"langId": "id"}})
     */
    public function editAction(Request $request, Language $language)
    {
        $form = $this->formFactory->create('knowledgebase_core_language', $language);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($language);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Language successfully updated');

            return new RedirectResponse($this->router->generate(
                'language_edit',
                array('langId' => $language->getId())
            ));
        }

        return array(
            'language' => $language,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/language/delete/{langId}", name="language_delete")
     * @Configuration\Template
     * @configuration\ParamConverter("language", options={"mapping": {"langId": "id"}})
     */
    public function deleteAction(Request $request, Language $language)
    {

        $this->entityManager->remove($language);
        $this->entityManager->flush();

        $this->flashBag->set('success', 'Language successfully deleted');

        return new RedirectResponse($this->router->generate('home'));
    }
}
