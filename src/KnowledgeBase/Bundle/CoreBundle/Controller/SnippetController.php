<?php
namespace KnowledgeBase\Bundle\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use KnowledgeBase\Bundle\CoreBundle\Entity\Snippet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SnippetController
 *
 * @Configuration\Route(service="knowledgebase_core.controller.snippet_controller")
 */
class SnippetController
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FlashBagInterface $flashBag
     * @param RouterInterface $router
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FlashBagInterface $flashBag,
        RouterInterface $router
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $this->router = $router;
    }

    /**
     * @Configuration\Route("/snippet/detail/{snippetId}", name="snippet_detail")
     * @Configuration\Template
     * @Configuration\ParamConverter("snippet", options={"mapping": {"snippetId": "id"}})
     */
    public function detailAction(Snippet $snippet)
    {
        return array(
            'snippet' => $snippet
        );
    }

    /**
     * @Configuration\Route("/snippet/create", name="snippet_create")
     * @Configuration\Template
     */
    public function createAction(Request $request)
    {
        $snippet = new Snippet();

        $form = $this->formFactory->create('knowledgebase_core_snippet', $snippet);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($snippet);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Snippet successfully created');

            return new RedirectResponse($this->router->generate(
                'snippet_edit',
                array('snippetId' => $snippet->getId())
            ));
        }


        return array(
            'snippet' => $snippet,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/snippet/edit/{snippetId}", name="snippet_edit")
     * @Configuration\Template
     * @configuration\ParamConverter("snippet", options={"mapping": {"snippetId": "id"}})
     */
    public function editAction(Request $request, Snippet $snippet)
    {
        $form = $this->formFactory->create('knowledgebase_core_snippet', $snippet);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($snippet);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Snippet successfully updated');

            return new RedirectResponse($this->router->generate(
                'snippet_detail',
                array('snippetId' => $snippet->getId())
            ));
        }

        return array(
            'snippet' => $snippet,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/snippet/delete/{snippetId}", name="snippet_delete")
     * @Configuration\Template
     * @configuration\ParamConverter("snippet", options={"mapping": {"snippetId": "id"}})
     */
    public function deleteAction(Request $request, Snippet $snippet)
    {

        $this->entityManager->remove($snippet);
        $this->entityManager->flush();

        $this->flashBag->set('success', 'Snippet successfully deleted');

        return new RedirectResponse($this->router->generate('home'));
    }
}
