<?php

namespace KnowledgeBase\Bundle\CoreBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use KnowledgeBase\Bundle\CoreBundle\Entity\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class TagController
 *
 * @Configuration\Route(service="knowledgebase_core.controller.tag_controller")
 */
class TagController {

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityRepository
     */
    private $tagRepository;

    /**
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FlashBagInterface $flashBag
     * @param RouterInterface $router
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FlashBagInterface $flashBag,
        RouterInterface $router,
        EntityRepository $tagRepository
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->tagRepository = $tagRepository;
    }


    /**
     * @Configuration\Route("/tags", name="tag_index")
     * @Configuration\Template
     *
     * @return array
     */
    public function indexAction() {
        $tags = $this->tagRepository->findAll();

        return array(
            'tags' => $tags
        );
    }

    /**
     * @Configuration\Route("/tag/create", name="tag_create")
     * @Configuration\Template
     *
     */
    public function createAction(Request $request) {
        $tag = new Tag();

        $form = $this->formFactory->create('knowledgebase_core_tag', $tag);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($tag);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Tag successfully created');

            return new RedirectResponse($this->router->generate(
                'tag_index'
            ));
        }

        return array(
            'tag' => $tag,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/tag/edit/{tagId}", name="tag_edit")
     * @Configuration\Template
     *
     * @Configuration\ParamConverter("tag", options={"mapping": {"tagId": "id"}})
     *
     */
    public function editAction(Request $request, Tag $tag) {
        $form = $this->formFactory->create('knowledgebase_core_tag', $tag);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->entityManager->persist($tag);
            $this->entityManager->flush();

            $this->flashBag->set('success', 'Tag successfully updated');

            return new RedirectResponse($this->router->generate(
                'tag_edit',
                array('tagId' => $tag->getId())
            ));
        }

        return array(
            'tag' => $tag,
            'form' => $form->createView()
        );
    }

    /**
     * @Configuration\Route("/tag/delete/{tagId}", name="tag_delete")
     * @Configuration\Template
     *
     * @Configuration\ParamConverter("tag", options={"mapping": {"tagId": "id"}})
     *
     */
    public function deleteAction(Request $request, Tag $tag) {
        $this->entityManager->remove($tag);
        $this->entityManager->flush();

        $this->flashBag->set('success', 'Tag successfully deleted');

        return new RedirectResponse($this->router->generate(
            'tag_index'
        ));
    }
}
