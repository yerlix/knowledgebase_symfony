<?php
namespace KnowledgeBase\Bundle\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 *
 * @ORM\Entity(repositoryClass="Doctrine\ORM\EntityRepository")
 * @ORM\Table(name="category")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var Snippet[]
     *
     * @ORM\OneToMany(targetEntity="Snippet", mappedBy="category")
     */
    protected $snippets;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="font_awesome_class")
     */
    protected $fontAwesomeClass;

    /**
     * Constructor
     */
    function __construct()
    {
        $this->snippets = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $fontAwesomeClass
     */
    public function setFontAwesomeClass($fontAwesomeClass)
    {
        $this->fontAwesomeClass = $fontAwesomeClass;
    }

    /**
     * @return string
     */
    public function getFontAwesomeClass()
    {
        return $this->fontAwesomeClass;
    }

    /**
     * @param Snippet $snippet
     */
    public function addSnippet(Snippet $snippet)
    {
        $this->snippets[] = $snippet;
    }

    /**
     * @param Snippet $snippet
     */
    public function removeSnippet(Snippet $snippet)
    {
        $this->snippets->removeElement($snippet);
    }

    /**
     * @return Snippet[]
     */
    public function getSnippets()
    {
        return $this->snippets;
    }

    function __toString()
    {
        return $this->getTitle();
    }
}
