<?php
namespace KnowledgeBase\Bundle\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Language
 *
 * @ORM\Entity
 * @ORM\Table(name="language")
 */
class Language
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Snippet[]
     *
     * @ORM\OneToMany(targetEntity="Snippet", mappedBy="language")
     */
    private $snippets;

    /**
     * Language constructor.
     */
    public function __construct()
    {
        $this->snippets = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

    }

    /**
     * @param Snippet $snippet
     */
    public function addSnippet(Snippet $snippet)
    {
        $this->snippets[] = $snippet;
    }

    /**
     * @param Snippet $snippet
     */
    public function removeSnippet(Snippet $snippet)
    {
        $this->snippets->removeElement($snippet);
    }

    /**
     * @return Snippet[]
     */
    public function getSnippets()
    {
        return $this->snippets;
    }
}
