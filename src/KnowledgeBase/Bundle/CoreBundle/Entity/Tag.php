<?php

namespace KnowledgeBase\Bundle\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 *
 * @ORM\Entity
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Snippet", mappedBy="tags")
     */
    private $snippets;

    function __construct()
    {
        $this->snippets = new ArrayCollection();
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param Snippet $snippet
     */
    public function addSnippet(Snippet $snippet)
    {
        $this->snippets[] = $snippet;
    }

    /**
     * @param Snippet $snippet
     */
    public function removeSnippet(Snippet $snippet)
    {
        $this->snippets->removeElement($snippet);
    }

    /**
     * @return Snippet[]
     */
    public function getSnippets()
    {
        return $this->snippets;
    }

    /**
     * {@inheritdoc}
     */
    function __toString()
    {
        return $this->getTitle();
    }
}
