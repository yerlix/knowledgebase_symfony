<?php

namespace KnowledgeBase\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('fontAwesomeClass');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'knowledgebase_core_category';
    }
}
