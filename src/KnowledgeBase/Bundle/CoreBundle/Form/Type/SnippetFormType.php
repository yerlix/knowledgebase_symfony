<?php

namespace KnowledgeBase\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SnippetFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('codeSnippet', 'textarea')
            ->add(
                'category',
                'entity',
                array(
                    'class' => 'KnowledgeBaseCoreBundle:Category',
                    'empty_value' => 'Choose a category'
                )
            )
            ->add(
                'tags',
                'entity',
                array(
                    'class' => 'KnowledgeBaseCoreBundle:Tag',
                    'multiple' => true,
                    'expanded' => false
                )
            )
            ->add(
                'language',
                'entity',
                array(
                    'class' => 'KnowledgeBaseCoreBundle:Language',
                    'property' => 'name',
                    'empty_value' => 'Choose a language'
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'knowledgebase_core_snippet';
    }
}
