<?php

namespace KnowledgeBase\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TagFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'knowledgebase_core_tag';
    }
}
