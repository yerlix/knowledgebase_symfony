<?php

namespace KnowledgeBase\Bundle\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * KnowledgeBase core functionality.
 */
class KnowledgeBaseCoreBundle extends Bundle
{
}
