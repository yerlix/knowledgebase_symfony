<?php

namespace KnowledgeBase\Bundle\CoreBundle\Menu;

use Doctrine\ORM\EntityRepository;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EntityRepository
     */
    private $categoryRepository;

    /**
     * @param FactoryInterface $factory
     * @param EntityRepository $categoryRepository
     */
    public function __construct(FactoryInterface $factory, EntityRepository $categoryRepository)
    {
        $this->factory = $factory;
        $this->categoryRepository = $categoryRepository;
    }

    public function createMainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->setChildrenAttributes(array('class' => 'nav nav-stacked'));

        $menu->addChild(
            'Home',
            array(
                'route' => 'home',
                'attributes' => array(
                    'icon' => 'fa fa-home'
                )
            )
        );

        $menu->addChild(
            'Snippets',
            array(
                'uri' => '#',
                'attributes' => array(
                    'icon' => 'fa fa-chevron-right',
                )
            )
        )
            ->setLinkAttribute('data-toggle', 'collapse')
            ->setLinkAttribute('data-target', '#userMenu')
            ->setChildrenAttributes(array('class' => 'nav nav-stacked collapse collapsed', 'id' => 'userMenu'));

        // TODO add foreach over all categories
        $categories = $this->categoryRepository->findAll();

        foreach ($categories as $category) {
            $menu['Snippets']->addChild(
                $category->getTitle(),
                array(
                    'route' => 'category_index',
                    'routeParameters' => array('categoryId' => $category->getId()),
                    'attributes' => array(
                        'icon' => $category->getFontAwesomeClass()
                    )
                )
            );
        }

        $menu['Snippets']->addChild(
            'Add new',
            array(
                'route' => 'category_create',
                'attributes' => array(
                    'icon' => 'fa fa-plus'
                )
            )
        );

        $menu->addChild(
            'Languages',
            array(
                'route' => 'language_index',
                'attributes' => array(
                    'icon' => 'fa fa-list'
                )
            )
        );

        $menu->addChild(
            'Tags',
            array(
                'route' => 'tag_index',
                'attributes' => array(
                    'icon' => 'fa fa-tags'
                )
            )
        );

        return $menu;
    }
}
